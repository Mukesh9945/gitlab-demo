import React from "react";
import "./header.css";
// IMAGES
// import logo from "../assets/images/logowm.png";

// ICONS
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import NotificationsNoneRoundedIcon from '@material-ui/icons/NotificationsNoneRounded';
import MenuRoundedIcon from '@material-ui/icons/MenuRounded';
import IconButton from "@material-ui/core/IconButton";

function Header() {
  return (
    <header className="header">
      <div className="header__logoContainer">
        {/* <a href="http://localhost:3000/">
          <img src={logo} alt="logo" className="header__logo" />
        </a> */}
      </div>
      <div className="header__accountMenuWrapper">
        <IconButton>
          <AccountCircleIcon />
        </IconButton>
      </div>
      <div className="header__notificationMenuWrapper">
        <IconButton>
          <NotificationsNoneRoundedIcon />
        </IconButton>
      </div>
      <div className="header__burgerMenuWrapper">
        <IconButton>
          <MenuRoundedIcon />
        </IconButton>
      </div>
    </header>
  );
}

export default Header;
