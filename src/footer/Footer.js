import React from "react";
import "./footer.css";

function Footer() {
  return (
    <div className="footer">
      <ul className="footer__AuxLink">
        <li>
          {" "}
          <a href="http://" target="_blank" rel="noopener noreferrer">
            Privacy and Cookies
          </a>{" "}
        </li>
        <li>
          <a href="http://" target="_blank" rel="noopener noreferrer">
            Legal
          </a>
        </li>
        <li>
          <a href="http://" target="_blank" rel="noopener noreferrer">
            Feedback
          </a>
        </li>
        <li>
          <a href="http://" target="_blank" rel="noopener noreferrer">
            Contact
          </a>
        </li>
      </ul>
      <div className="footer__copywrite">
        <p>© 2020 wisemonkeys.</p>
      </div>
    </div>
  );
}

export default Footer;
