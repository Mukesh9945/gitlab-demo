import React, { useEffect } from "react";
import WmCards from "../wmCards/WmCards";
import "./wmCarousel.css";
import $ from "jquery";
// ICONS
import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";

// IMAGE
import carddemo from "../../assets/images/carddemo.jpeg";
import { Container, Row } from "react-bootstrap";

function WmCarousel() {
  const imageRef = React.useRef(null);
  useEffect(() => {
    $(".wmbody_image").mouseenter(function () {
      $(this).siblings(".wmcard_icon").addClass("wmIcon_display");
      $(this).siblings(".wmbody_description").addClass("wmDescription_display");
    });
    $(".wmcard_body").mouseleave(function () {
      $(this).children(".wmcard_icon").removeClass("wmIcon_display");
      $(this)
        .children(".wmbody_description")
        .removeClass("wmDescription_display");
    });
  }, []);

  return (
    <div>
      <div
        id="carouselLatestBlogControls"
        className="carousel slide"
        data-bs-ride="carousel"
      >
        <div className="carousel-inner">
          <div className="carousel-item active">
            <Container fluid>
              <Row>
                <WmCards>
                  <div className={`wmcard_body`}>
                    <IconButton className={`wmcard_icon`}>
                      <MoreVertIcon />
                    </IconButton>
                    <img
                      src={carddemo}
                      alt="carddemo"
                      className="wmbody_image"
                      ref={imageRef}
                    />
                    <div className={`wmbody_description`}>
                      <div className="wmbody__descriptionPara">
                        This impressive paella is a perfect party dish and a fun
                        meal to cook together with your guests. Add 1 cup of
                        frozen peas along with the mussels, if you like.
                      </div>
                      <div>
                        <IconButton>
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton>
                          <ShareIcon />
                        </IconButton>
                      </div>
                    </div>
                  </div>
                </WmCards>
                <WmCards>
                  <div className={`wmcard_body`}>
                    <IconButton className={`wmcard_icon`}>
                      <MoreVertIcon />
                    </IconButton>
                    <img
                      src={carddemo}
                      alt="carddemo"
                      className="wmbody_image"
                      ref={imageRef}
                    />
                    <div className={`wmbody_description`}>
                      <div className="wmbody__descriptionPara">
                        This impressive paella is a perfect party dish and a fun
                        meal to cook together with your guests. Add 1 cup of
                        frozen peas along with the mussels, if you like.
                      </div>
                      <div>
                        <IconButton>
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton>
                          <ShareIcon />
                        </IconButton>
                      </div>
                    </div>
                  </div>
                </WmCards>
                <WmCards>
                  <div className={`wmcard_body`}>
                    <IconButton className={`wmcard_icon`}>
                      <MoreVertIcon />
                    </IconButton>
                    <img
                      src={carddemo}
                      alt="carddemo"
                      className="wmbody_image"
                      ref={imageRef}
                    />
                    <div className={`wmbody_description`}>
                      <div className="wmbody__descriptionPara">
                        This impressive paella is a perfect party dish and a fun
                        meal to cook together with your guests. Add 1 cup of
                        frozen peas along with the mussels, if you like.
                      </div>
                      <div>
                        <IconButton>
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton>
                          <ShareIcon />
                        </IconButton>
                      </div>
                    </div>
                  </div>
                </WmCards>
                <WmCards>
                  <div className={`wmcard_body`}>
                    <IconButton className={`wmcard_icon`}>
                      <MoreVertIcon />
                    </IconButton>
                    <img
                      src={carddemo}
                      alt="carddemo"
                      className="wmbody_image"
                      ref={imageRef}
                    />
                    <div className={`wmbody_description`}>
                      <div className="wmbody__descriptionPara">
                        This impressive paella is a perfect party dish and a fun
                        meal to cook together with your guests. Add 1 cup of
                        frozen peas along with the mussels, if you like.
                      </div>
                      <div>
                        <IconButton>
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton>
                          <ShareIcon />
                        </IconButton>
                      </div>
                    </div>
                  </div>
                </WmCards>
              </Row>
            </Container>
          </div>
          <div className="carousel-item">
            <Container fluid>
              <Row>
                <WmCards>
                  <div className={`wmcard_body`}>
                    <IconButton className={`wmcard_icon`}>
                      <MoreVertIcon />
                    </IconButton>
                    <img
                      src={carddemo}
                      alt="carddemo"
                      className="wmbody_image"
                      ref={imageRef}
                    />
                    <div className={`wmbody_description`}>
                      <div className="wmbody__descriptionPara">
                        This impressive paella is a perfect party dish and a fun
                        meal to cook together with your guests. Add 1 cup of
                        frozen peas along with the mussels, if you like.
                      </div>
                      <div>
                        <IconButton>
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton>
                          <ShareIcon />
                        </IconButton>
                      </div>
                    </div>
                  </div>
                </WmCards>
                <WmCards>
                  <div className={`wmcard_body`}>
                    <IconButton className={`wmcard_icon`}>
                      <MoreVertIcon />
                    </IconButton>
                    <img
                      src={carddemo}
                      alt="carddemo"
                      className="wmbody_image"
                      ref={imageRef}
                    />
                    <div className={`wmbody_description`}>
                      <div className="wmbody__descriptionPara">
                        This impressive paella is a perfect party dish and a fun
                        meal to cook together with your guests. Add 1 cup of
                        frozen peas along with the mussels, if you like.
                      </div>
                      <div>
                        <IconButton>
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton>
                          <ShareIcon />
                        </IconButton>
                      </div>
                    </div>
                  </div>
                </WmCards>
                <WmCards>
                  <div className={`wmcard_body`}>
                    <IconButton className={`wmcard_icon`}>
                      <MoreVertIcon />
                    </IconButton>
                    <img
                      src={carddemo}
                      alt="carddemo"
                      className="wmbody_image"
                      ref={imageRef}
                    />
                    <div className={`wmbody_description`}>
                      <div className="wmbody__descriptionPara">
                        This impressive paella is a perfect party dish and a fun
                        meal to cook together with your guests. Add 1 cup of
                        frozen peas along with the mussels, if you like.
                      </div>
                      <div>
                        <IconButton>
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton>
                          <ShareIcon />
                        </IconButton>
                      </div>
                    </div>
                  </div>
                </WmCards>
                <WmCards>
                  <div className={`wmcard_body`}>
                    <IconButton className={`wmcard_icon`}>
                      <MoreVertIcon />
                    </IconButton>
                    <img
                      src={carddemo}
                      alt="carddemo"
                      className="wmbody_image"
                      ref={imageRef}
                    />
                    <div className={`wmbody_description`}>
                      <div className="wmbody__descriptionPara">
                        This impressive paella is a perfect party dish and a fun
                        meal to cook together with your guests. Add 1 cup of
                        frozen peas along with the mussels, if you like.
                      </div>
                      <div>
                        <IconButton>
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton>
                          <ShareIcon />
                        </IconButton>
                      </div>
                    </div>
                  </div>
                </WmCards>
              </Row>
            </Container>
          </div>
          <div className="carousel-item">
            <Container fluid>
              <Row>
                <WmCards>
                  <div className={`wmcard_body`}>
                    <IconButton className={`wmcard_icon`}>
                      <MoreVertIcon />
                    </IconButton>
                    <img
                      src={carddemo}
                      alt="carddemo"
                      className="wmbody_image"
                      ref={imageRef}
                    />
                    <div className={`wmbody_description`}>
                      <div className="wmbody__descriptionPara">
                        This impressive paella is a perfect party dish and a fun
                        meal to cook together with your guests. Add 1 cup of
                        frozen peas along with the mussels, if you like.
                      </div>
                      <div>
                        <IconButton>
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton>
                          <ShareIcon />
                        </IconButton>
                      </div>
                    </div>
                  </div>
                </WmCards>
                <WmCards>
                  <div className={`wmcard_body`}>
                    <IconButton className={`wmcard_icon`}>
                      <MoreVertIcon />
                    </IconButton>
                    <img
                      src={carddemo}
                      alt="carddemo"
                      className="wmbody_image"
                      ref={imageRef}
                    />
                    <div className={`wmbody_description`}>
                      <div className="wmbody__descriptionPara">
                        This impressive paella is a perfect party dish and a fun
                        meal to cook together with your guests. Add 1 cup of
                        frozen peas along with the mussels, if you like.
                      </div>
                      <div>
                        <IconButton>
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton>
                          <ShareIcon />
                        </IconButton>
                      </div>
                    </div>
                  </div>
                </WmCards>
                <WmCards>
                  <div className={`wmcard_body`}>
                    <IconButton className={`wmcard_icon`}>
                      <MoreVertIcon />
                    </IconButton>
                    <img
                      src={carddemo}
                      alt="carddemo"
                      className="wmbody_image"
                      ref={imageRef}
                    />
                    <div className={`wmbody_description`}>
                      <div className="wmbody__descriptionPara">
                        This impressive paella is a perfect party dish and a fun
                        meal to cook together with your guests. Add 1 cup of
                        frozen peas along with the mussels, if you like.
                      </div>
                      <div>
                        <IconButton>
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton>
                          <ShareIcon />
                        </IconButton>
                      </div>
                    </div>
                  </div>
                </WmCards>
                <WmCards>
                  <div className={`wmcard_body`}>
                    <IconButton className={`wmcard_icon`}>
                      <MoreVertIcon />
                    </IconButton>
                    <img
                      src={carddemo}
                      alt="carddemo"
                      className="wmbody_image"
                      ref={imageRef}
                    />
                    <div className={`wmbody_description`}>
                      <div className="wmbody__descriptionPara">
                        This impressive paella is a perfect party dish and a fun
                        meal to cook together with your guests. Add 1 cup of
                        frozen peas along with the mussels, if you like.
                      </div>
                      <div>
                        <IconButton>
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton>
                          <ShareIcon />
                        </IconButton>
                      </div>
                    </div>
                  </div>
                </WmCards>
              </Row>
            </Container>
          </div>
        </div>
        <div className="wmCarousel_controllerWrapper">
          <IconButton
            data-bs-target="#carouselLatestBlogControls"
            data-bs-slide="prev"
          >
            <ArrowBackIcon />
          </IconButton>
          <IconButton
            data-bs-target="#carouselLatestBlogControls"
            data-bs-slide="next"
          >
            <ArrowForwardIcon />
          </IconButton>
        </div>
      </div>
    </div>
  );
}

export default WmCarousel;
