import React from 'react'
import "./WmCards.css";

function WmCards(props) {
    return (
        <div className={`wmcards ${props.customCss}`}>
            {props.children}
        </div>
    )
}

export default WmCards
