import React, { Component } from "react";
import OwlCarousel from "react-owl-carousel2";
import "../../node_modules/react-owl-carousel2/src/owl.carousel.css";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
//ICONS
import IconButton from "@material-ui/core/IconButton";

export class Owl extends Component {
  render() {
    const options = {
      items: this.props.cardNumber,
      rewind: true,
      margin: 20,
      mouseDrag:false,
    };
    return (
      <div>
        <OwlCarousel ref="car" options={options}>
          {this.props.children}
        </OwlCarousel>
        <div className="owl_controls_Wrapper">
          <IconButton onClick={() => this.refs.car.prev()}>
            <ArrowBackIcon />
          </IconButton>
          <IconButton onClick={() => this.refs.car.next()}>
            <ArrowForwardIcon />
          </IconButton>
        </div>
      </div>
    );
  }
}

export default Owl;
