import React, { useEffect } from "react";
import "./App.css";
import Header from "./header/Header";
import Content from "./LandingPageContent/Content";
import Search from "./Search/Search";
import "bootstrap/dist/css/bootstrap.min.css";
import $ from "jquery";
// import { TweenMax } from "gsap";
// import gsap from "gsap/gsap-core";
// import { gsap } from "gsap/dist/gsap";
// import { ScrollTrigger } from "gsap/ScrollTrigger.js";
// gsap.registerPlugin(ScrollTrigger);

function App() {
  useEffect(() => {
    $(window).scroll(function () {
      var scrolled_val = $(document).scrollTop().valueOf();
      var y_scroll_pos = window.pageYOffset;
      var scroll_pos_test = 238;
      var num = 10;
      if (y_scroll_pos > scroll_pos_test) {
        $(".search__form").css({ top: `${num += 1}%` });
      }
      else{
        $(".search__form").css({ top: "" });
      }
      if (scrolled_val > 700) {
        $(".content").css({ height: "80vh", overflow: "auto" });
      }
      if (scrolled_val < 700) {
        $(".content").css({ height: "", overflow: "" });
      }
    });
  }, []);
  // useEffect(() => {
  //   gsap.to(".search__form", {
  //     scrollTrigger: {
  //       trigger: ".content",
  //       toggleActions: "play pause reverse reset",
  //       start: "top 70%",
  //       markers: true,
  //     },
  //     y: -200,
  //     duration: 1,
  //   });
  // }, []);
  const searchRef = (ref) => {
    console.log(ref);
  };
  return (
    <React.Fragment>
      <div className="app">
        <Header />
        <Search setSearchFormRef={searchRef} />
      </div>
      <Content />
    </React.Fragment>
  );
}

export default App;
