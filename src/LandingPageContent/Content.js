import React from "react";
import Blogs from "./Blogs/Blogs";
import "./content.css";
import Footer from "../footer/Footer";
import Polls from "./Polls/Polls";
import Profile from "./Profile/Profile";


function Content() {
  return (
    <div className="content">
      <div className="content_container">
        <Blogs />
        
        <Polls />
        <Profile />
      </div>
      <Footer />
    </div>
  );
}

export default Content;
