import React, { useEffect } from "react";
import "./blogs.css";
import $ from "jquery";

import Owl from "../../Components/Owl";
import WmCards from "../../Components/wmCards/WmCards";
// ICONS
import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";

import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
// IMAGE
import carddemo from "../../assets/images/carddemo.jpeg";

function Blogs() {
  const imageRef = React.useRef(null);
  useEffect(() => {
    $(".wmbody_image").mouseenter(function () {
      $(this).siblings(".wmcard_icon").addClass("wmIcon_display");
      $(this).siblings(".wmbody_description").addClass("wmDescription_display");
    });
    $(".wmcard_body").mouseleave(function () {
      $(this).children(".wmcard_icon").removeClass("wmIcon_display");
      $(this)
        .children(".wmbody_description")
        .removeClass("wmDescription_display");
    });
  }, []);
  return (
    <div className="blogs contentGlobal">
      <h4>Latest Blogs</h4>
      <Owl>
        <WmCards customCss={"blogs_card"}>
          <div className={`wmcard_body`} style={{backgroundImage: `url(${carddemo})`}}>
            <IconButton className={`wmcard_icon`}>
              <MoreVertIcon />
            </IconButton>
            <img
              src={carddemo}
              alt="carddemo"
              className="wmbody_image"
              ref={imageRef}
            />
            <div className={`wmbody_description`}>
              <div className="wmbody__descriptionPara ">
                This impressive paella is a perfect party dish and a fun meal to
                cook together with your guests. Add 1 cup of frozen peas along
                with the mussels, if you like.
              </div>
              <div>
                <IconButton>
                  <FavoriteIcon />
                </IconButton>
                <IconButton>
                  <ShareIcon />
                </IconButton>
              </div>
            </div>
          </div>
        </WmCards>
        <WmCards customCss={"blogs_card"}>
          <div className={`wmcard_body`}>
            <IconButton className={`wmcard_icon`}>
              <MoreVertIcon />
            </IconButton>
            <img
              src={carddemo}
              alt="carddemo"
              className="wmbody_image"
              ref={imageRef}
            />
            <div className={`wmbody_description`}>
              <div className="wmbody__descriptionPara">
                This impressive paella is a perfect party dish and a fun meal to
                cook together with your guests. Add 1 cup of frozen peas along
                with the mussels, if you like.
              </div>
              <div>
                <IconButton>
                  <FavoriteIcon />
                </IconButton>
                <IconButton>
                  <ShareIcon />
                </IconButton>
              </div>
            </div>
          </div>
        </WmCards>
        <WmCards customCss={"blogs_card"}>
          <div className={`wmcard_body`}>
            <IconButton className={`wmcard_icon`}>
              <MoreVertIcon />
            </IconButton>
            <img
              src={carddemo}
              alt="carddemo"
              className="wmbody_image"
              ref={imageRef}
            />
            <div className={`wmbody_description`}>
              <div className="wmbody__descriptionPara">
                This impressive paella is a perfect party dish and a fun meal to
                cook together with your guests. Add 1 cup of frozen peas along
                with the mussels, if you like.
              </div>
              <div>
                <IconButton>
                  <FavoriteIcon />
                </IconButton>
                <IconButton>
                  <ShareIcon />
                </IconButton>
              </div>
            </div>
          </div>
        </WmCards>
        <WmCards customCss={"blogs_card"}>
          <div className={`wmcard_body`}>
            <IconButton className={`wmcard_icon`}>
              <MoreVertIcon />
            </IconButton>
            <img
              src={carddemo}
              alt="carddemo"
              className="wmbody_image"
              ref={imageRef}
            />
            <div className={`wmbody_description`}>
              <div className="wmbody__descriptionPara">
                This impressive paella is a perfect party dish and a fun meal to
                cook together with your guests. Add 1 cup of frozen peas along
                with the mussels, if you like.
              </div>
              <div>
                <IconButton>
                  <FavoriteIcon />
                </IconButton>
                <IconButton>
                  <ShareIcon />
                </IconButton>
              </div>
            </div>
          </div>
        </WmCards>
        <WmCards customCss={"blogs_card"}>
          <div className={`wmcard_body`}>
            <IconButton className={`wmcard_icon`}>
              <MoreVertIcon />
            </IconButton>
            <img
              src={carddemo}
              alt="carddemo"
              className="wmbody_image"
              ref={imageRef}
            />
            <div className={`wmbody_description`}>
              <div className="wmbody__descriptionPara">
                This impressive paella is a perfect party dish and a fun meal to
                cook together with your guests. Add 1 cup of frozen peas along
                with the mussels, if you like.
              </div>
              <div>
                <IconButton>
                  <FavoriteIcon />
                </IconButton>
                <IconButton>
                  <ShareIcon />
                </IconButton>
              </div>
            </div>
          </div>
        </WmCards>
      </Owl>
    </div>
  );
}

export default Blogs;
