import React from "react";
import Owl from "../../Components/Owl";
import WmCards from "../../Components/wmCards/WmCards";
import "./profile.css";

// IMAGE
import dummyimage from "../../assets/images/carddemo.jpeg";

// ICONS

import RemoveRedEyeOutlinedIcon from "@material-ui/icons/RemoveRedEyeOutlined";
import FavoriteBorderOutlinedIcon from "@material-ui/icons/FavoriteBorderOutlined";
import PersonOutlinedIcon from "@material-ui/icons/PersonOutlined";

function Profile() {
  //
  return (
    <div className="profile contentGlobal">
      <h4>Trending Profile</h4>
      <Owl cardNumber={3}>
        <WmCards customCss={"profilecard_main"}>
          <div className="profilecard__head">
            <div className="profilecard_headLeft">
              <h5>Mukesh Swamy</h5>
              <small>Web Developer</small>
            </div>
            <div className="profilecard_headRight">
              <img src={dummyimage} alt="profile" />
            </div>
          </div>
          <div className="profile__footer">
            <div className="profile__userStats">
              <div>
                <FavoriteBorderOutlinedIcon />
                Likes
              </div>
              <div>100</div>
            </div>
            <div className="profile__userStats">
              <div>
                <PersonOutlinedIcon />
                Followers
              </div>
              <div>100</div>
            </div>
            <div className="profile__userStats">
              <div>
                <RemoveRedEyeOutlinedIcon />
                views
              </div>
              <div>100</div>
            </div>
          </div>
        </WmCards>
        <WmCards customCss={"profilecard_main"}>
          <div className="profilecard__head">
            <div className="profilecard_headLeft">
              <h5>Mukesh Swamy</h5>
              <small>Web Developer</small>
            </div>
            <div className="profilecard_headRight">
              <img src={dummyimage} alt="profile" />
            </div>
          </div>
          <div className="profile__footer">
            <div className="profile__userStats">
              <div>
                <FavoriteBorderOutlinedIcon />
                Likes
              </div>
              <div>100</div>
            </div>
            <div className="profile__userStats">
              <div>
                <PersonOutlinedIcon />
                Followers
              </div>
              <div>100</div>
            </div>
            <div className="profile__userStats">
              <div>
                <RemoveRedEyeOutlinedIcon />
                views
              </div>
              <div>100</div>
            </div>
          </div>
        </WmCards>
        <WmCards customCss={"profilecard_main"}>
          <div className="profilecard__head">
            <div className="profilecard_headLeft">
              <h5>Mukesh Swamy</h5>
              <small>Web Developer</small>
            </div>
            <div className="profilecard_headRight">
              <img src={dummyimage} alt="profile" />
            </div>
          </div>
          <div className="profile__footer">
            <div className="profile__userStats">
              <div>
                <FavoriteBorderOutlinedIcon />
                Likes
              </div>
              <div>100</div>
            </div>
            <div className="profile__userStats">
              <div>
                <PersonOutlinedIcon />
                Followers
              </div>
              <div>100</div>
            </div>
            <div className="profile__userStats">
              <div>
                <RemoveRedEyeOutlinedIcon />
                views
              </div>
              <div>100</div>
            </div>
          </div>
        </WmCards>
        <WmCards customCss={"profilecard_main"}>
          <div className="profilecard__head">
            <div className="profilecard_headLeft">
              <h5>Mukesh Swamy</h5>
              <small>Web Developer</small>
            </div>
            <div className="profilecard_headRight">
              <img src={dummyimage} alt="profile" />
            </div>
          </div>
          <div className="profile__footer">
            <div className="profile__userStats">
              <div>
                <FavoriteBorderOutlinedIcon />
                Likes
              </div>
              <div>100</div>
            </div>
            <div className="profile__userStats">
              <div>
                <PersonOutlinedIcon />
                Followers
              </div>
              <div>100</div>
            </div>
            <div className="profile__userStats">
              <div>
                <RemoveRedEyeOutlinedIcon />
                views
              </div>
              <div>100</div>
            </div>
          </div>
        </WmCards>
      </Owl>
    </div>
  );
}

export default Profile;
