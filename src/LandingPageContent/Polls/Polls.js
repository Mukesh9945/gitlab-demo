import React from "react";
import "./polls.css";
import WmCards from "../../Components/wmCards/WmCards";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
// ICONS
import Avatar from "@material-ui/core/Avatar";
import Owl from "../../Components/Owl";

function Polls() {
  return (
    <div className="polls contentGlobal">
      <h4>Latest Polls</h4>
      <Owl cardNumber={2}>
        <WmCards customCss={"polls_card"}>
          <Card>
            <CardHeader
              avatar={<Avatar aria-label="recipe">M</Avatar>}
              title="Shrimp and Chorizo Paella"
              subheader="September 14, 2016"
            />
            <CardContent>
              <h5>
                orem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </h5>
            </CardContent>
          </Card>
        </WmCards>
        <WmCards customCss={"polls_card"}>
          <Card>
            <CardHeader
              avatar={<Avatar aria-label="recipe">M</Avatar>}
              title="Shrimp and Chorizo Paella"
              subheader="September 14, 2016"
            />
            <CardContent>
              <h5>
                orem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </h5>
            </CardContent>
          </Card>
        </WmCards>
        <WmCards customCss={"polls_card"}>
          <Card>
            <CardHeader
              avatar={<Avatar aria-label="recipe">M</Avatar>}
              title="Shrimp and Chorizo Paella"
              subheader="September 14, 2016"
            />
            <CardContent>
              <h5>
                orem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </h5>
            </CardContent>
          </Card>
        </WmCards>
        <WmCards customCss={"polls_card"}>
          <Card>
            <CardHeader
              avatar={<Avatar aria-label="recipe">M</Avatar>}
              title="Shrimp and Chorizo Paella"
              subheader="September 14, 2016"
            />
            <CardContent>
              <h5>
                orem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </h5>
            </CardContent>
          </Card>
        </WmCards>
        <WmCards customCss={"polls_card"}>
          <Card>
            <CardHeader
              avatar={<Avatar aria-label="recipe">M</Avatar>}
              title="Shrimp and Chorizo Paella"
              subheader="September 14, 2016"
            />
            <CardContent>
              <h5>
                orem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </h5>
            </CardContent>
          </Card>
        </WmCards>
      </Owl>
    </div>
  );
}

export default Polls;
