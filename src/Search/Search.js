import React, { useState } from "react";
import "./search.css";
// ICON
import SearchRoundedIcon from "@material-ui/icons/SearchRounded";

function Search(props) {
  const [isShowSearchSuggestion, setIsShowSearchSuggestion] = useState(false);
  const toggelsearchSuggestion = (e) => {
    // console.log(e);
    setIsShowSearchSuggestion(!isShowSearchSuggestion);
  };
  return (
    <div className="search">
      <form className="search__form" ref={props.setSearchFormRef}>
        <div
          className={`search__inputWrapper ${
            isShowSearchSuggestion ? "search__barRemoveBorder" : ""
          }`}
          onBlur={(e) => toggelsearchSuggestion(e)}
        >
          {/* START WITH DEBOUNCE */}
          <input
            type="text"
            placeholder="search"
            onFocus={(e) => toggelsearchSuggestion(e)}
          />
          <span className="search__iconWrapper">
            <SearchRoundedIcon />
          </span>
        </div>
        <div
          className={`search__suggestionBox ${
            isShowSearchSuggestion ? "search__suggestionBoxDisplay" : ""
          }`}
        >
          <div>
            <a href="http://localhost:3000/"> Search 1 </a>
          </div>
          <div>
            <a href="http://localhost:3000/"> Search 2</a>
          </div>
          <div>
            <a href="http://localhost:3000/"> Search 3</a>
          </div>

          <a href="http://localhost:3000/" className="search__seemore">see more</a>
        </div>
      </form>
    </div>
  );
}

export default Search;
